// ----------------------------------------------------------------------------
//FIXME put into w3strings file asap
function rqfg_GetLocStringByKeyExt(key: string) : String {
    var result: String;

    switch (key) {
    // -- top level mod stuff
        case "RADFG_ModName": return "Radish Foliage-Generator";
        case "RADFG_Started": return "Radish Foliage-Generator started";
        case "RADFG_Stopped": return "Radish Foliage-Generator stopped";

        // Help
        case "RADFG_tHelpHotkey": return "Radish Foliage-Generator - Hotkey Help";
        case "RADFG_iGenerationStopped": return "foliage generating stopped unexpected. retry to continue.";
        case "RADFG_iGenerationDone": return "foliage generation finished.";
        case "RADFG_iClearDone": return "all spawned (preview) foliage removed.";
        case "RADFG_iDefinitionLogged": return "foliage definition logged.";

        // -- quit mod
        case "RADFG_tQuitConfirmUnsaved": return "Radish Foliage-Generator - Unsaved Changes";
        case "RADFG_mQuitConfirmUnsaved": return "You have unsaved changes. Do you really want to QUIT?";
        case "RADFG_tQuitConfirm": return "Radish Foliage-Generator";
        case "RADFG_mQuitConfirm": return "Do you really want to QUIT?";

        // -- general help
            case "RADFG_ShowHelp": return "Show this help";
            case "RADFG_mGeneralHelp": return "Generate randomized foliage (trees, grass, stones) based on terrain height.";
            case "RADFG_Minimize": return "Minimize Foliage-Generator (spawned foliage will stay spawned).";

            case "RADFG_LogDefinition": return "Log foliage definition for encoder.";
            case "RADFG_Quit": return "Quit Radish Foliage-Generator Mod (spawned foliage will be removed).";

        default:
            result = GetLocStringByKeyExt(key);
            if (result == "") {
                result = "#" + key;
            }
            return result;
    }
}
// ----------------------------------------------------------------------------
