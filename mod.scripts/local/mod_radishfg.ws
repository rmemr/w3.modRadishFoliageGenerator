// ----------------------------------------------------------------------------
class CRadFgPopupCallback extends IModUiConfirmPopupCallback {
    public var callback: CRadishFoliageGeneratorMod;

    public function OnConfirmed(action: String) {
        switch (action) {
            case "quit": return callback.doQuit();
        }
    }
}
// ----------------------------------------------------------------------------
state RadFG_Active in CRadishFoliageGeneratorMod {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var s: SRadishPlacement;
        var distance: float;

        parent.modEnv.deactivateHud();
        parent.modEnv.hidePlayer();
        parent.modEnv.freezeTime();

        // refresh camera position to player position
        s.pos = thePlayer.GetWorldPosition();
        s.rot = thePlayer.GetWorldRotation();
        s.rot.Pitch -= 25;
        s.rot.Roll = 0;
        distance = -5.0;
        s.pos.X -= distance * SinF(Deg2Rad(s.rot.Yaw - 15));
        s.pos.Y += distance * CosF(Deg2Rad(s.rot.Yaw - 15));
        s.pos.Z += 4.0;
        s.pos.W = 1.0;

        parent.configManager.setLastCamPosition(s);

        parent.radFg.activate();
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theInput.SetContext('Exploration');
        parent.radFg.deactivate();

        parent.modEnv.unfreezeTime();
        parent.modEnv.restorePlayer();
        parent.modEnv.reactivateHud();
    }
    // ------------------------------------------------------------------------
    event OnMinimize(action: SInputAction) {
        if (IsPressed(action)) {
            if (action.aName == 'RADFG_Minimize') {
                parent.GotoState('RadFG_Sleeping', false, true);
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadFG_Sleeping in CRadishFoliageGeneratorMod {
    // ------------------------------------------------------------------------
    event OnMaximize(action: SInputAction) {
        var entity : CEntity;
        var template : CEntityTemplate;

        if (IsPressed(action)) {
            if (!parent.radFg) {
                template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishfoliagegenerator\radfg.w3mod", true);
                entity = theGame.CreateEntity(template,
                    thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

                parent.radFg = (CRadishFoliageGenerator)entity;
                parent.radFg.init(parent.log, parent.configManager, parent.quitConfirmCallback);

                template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\radish_modutils.w2ent", true);
                entity = theGame.CreateEntity(template,
                    thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());
                parent.modEnv = (CRadishModUtils)entity;
            }

            parent.PushState('RadFG_Active');
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
statemachine class CRadishFoliageGeneratorMod extends CMod {
    default modName = 'RadishFoliageGenerator';
    default modAuthor = "rmemr";
    default modUrl = "http://www.nexusmods.com/witcher3/mods/xxxx/";
    default modVersion = '0.0.1';

    default logLevel = MLOG_DEBUG;
    // ------------------------------------------------------------------------
    protected var radFg: CRadishFoliageGenerator;

    protected var modEnv: CRadishModUtils;
    // ------------------------------------------------------------------------
    // UI stuff
    protected var quitConfirmCallback: CRadFgPopupCallback;
    // ------------------------------------------------------------------------
    //private var modConfigId: CName; default modConfigId = 'RadishUiConfig';
    protected var configManager: CRadishFoliageGeneratorConfigManager;
    // ------------------------------------------------------------------------
    public function init() {
        super.init();

        configManager = new CRadishFoliageGeneratorConfigManager in this;
        configManager.init();
        //configManager.init(this.log, (C...ConfigData)GetModStorage().load(modConfigId));

        this.registerListeners();

        // prepare view callback wiring
        quitConfirmCallback = new CRadFgPopupCallback in this;
        quitConfirmCallback.callback = this;

        PushState('RadFG_Sleeping');
    }
    // ------------------------------------------------------------------------
    event OnMinimize(action: SInputAction) {}
    event OnMaximize(action: SInputAction) {}
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnMinimize', 'RADFG_Minimize');
        theInput.RegisterListener(this, 'OnMaximize', 'RADFG_Maximize');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'RADFG_Minimize');
        theInput.UnregisterListener(this, 'RADFG_Maximize');
    }
    // ------------------------------------------------------------------------
    public function doQuit() {
        var null: CRadishFoliageGenerator;

        this.radFg.doQuit();
        this.radFg = null;
        PopState();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
function modCreate_RadishFoliageGenerator() : CRadishFoliageGeneratorMod {
    return new CRadishFoliageGeneratorMod in thePlayer;
}
// ----------------------------------------------------------------------------
