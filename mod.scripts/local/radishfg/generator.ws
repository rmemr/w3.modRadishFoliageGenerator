// ----------------------------------------------------------------------------
state RadFg_Active in CRadishFoliageGenerator {
    var stateLeaving: bool;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        stateLeaving = false;
        registerListeners();

        if (parent.foliagePatches.Size() == 0) {
            this.setupFoliagePatches();
        }
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        stateLeaving = true;
        unregisterListeners();
    }
    // ------------------------------------------------------------------------
    event OnInteractiveCam(action: SInputAction) {
        if (IsReleased(action)) {
            parent.PushState('RadFg_InteractiveCamera');
        }
    }
    // ------------------------------------------------------------------------
    event OnClear(action: SInputAction) {
        if (IsReleased(action)) {
            clearFoliage();
        }
    }
    // ------------------------------------------------------------------------
    private entry function clearFoliage() {
        var i, s: int;

        s = parent.foliagePatches.Size();
        for (i = 0; i < s; i += 1) {
            parent.foliagePatches[i].clear();
            SleepOneFrame();
        }
        parent.notice(rqfg_GetLocStringByKeyExt("RADFG_iClearDone"));
    }
    // ------------------------------------------------------------------------
    event OnGenerate(action: SInputAction) {
        if (IsReleased(action)) {
            generate();
        }
    }
    // ------------------------------------------------------------------------
    private function setupFoliagePatches() {
        var x, y: int;
        var patch: CRadFoliagePatch;
        var flip: bool;

        for (y = parent.area.y1; y < parent.area.y2; y += parent.patchSize) {
            // create patches in folowing order: left -> right -> left
            // to remove unnecessary "printer line-feeding" ;)
            if (flip) {
                for (x = parent.area.x2 - parent.patchSize; x >= parent.area.x1; x -= parent.patchSize) {

                    patch = new CRadFoliagePatch in this;
                    if (patch.init(x, y, parent.patchSize, parent.generatorParams)) {
                        parent.foliagePatches.PushBack(patch);
                    }
                }
            } else {
                for (x = parent.area.x1; x < parent.area.x2; x += parent.patchSize) {
                    patch = new CRadFoliagePatch in this;
                    if (patch.init(x, y, parent.patchSize, parent.generatorParams)) {
                        parent.foliagePatches.PushBack(patch);
                    }
                }
            }
            flip = !flip;
        }
    }
    // ------------------------------------------------------------------------
    private function onGenerationStopped(prevPosition: Vector, msgId: String) {
        // restore player position
        thePlayer.Teleport(prevPosition);
        parent.theCam.switchTo();
        parent.getCamTracker().resume();
        parent.notice(rqfg_GetLocStringByKeyExt(msgId));
    }
    // ------------------------------------------------------------------------
    private entry function generate() {
        var patch: CRadFoliagePatch;
        var tile: CRadFoliageMiniTile;
        var i, s: int;
        var patchFinished: bool;
        var prevPosition: Vector;
        var terrainProbe: CRadishTerrainProbe;

        terrainProbe = new CRadishTerrainProbe in this;

        parent.getCamTracker().stop();
        prevPosition = thePlayer.GetWorldPosition();

        s = parent.foliagePatches.Size();
        //LogChannel('FG', "generating patches " + s);
        for (i = 0; i < s; i += 1) {
            //LogChannel('FG', "generating patch: " + i + "(" + stateLeaving + ")");
            patch = parent.foliagePatches[i];
            patchFinished = patch.isDone() || !patch.newIter();

            if (!patchFinished && !stateLeaving) {
                // move cam to center of patch while refreshing terrainCollision
                terrainProbe.refreshTerrainCollisionWithoutBlackscreen(
                    patch.getCenter(), false, parent.theCam);

                //if (!terrainProbe.wasLastOpSuccessful()) {
                //    this.onGenerationStopped(prevPosition, "RADFG_iGenerationStopped");
                //    return;
                //}
            }
            while (!patchFinished && !stateLeaving) {
                tile = patch.next();
                //LogChannel('FG', "generating tile " + tile.getId());
                if (tile && !tile.isDone()) {
                    // pro actively refresh terrainCollision in center of tile
                    terrainProbe.refreshTerrainCollisionWithoutBlackscreen(tile.getCenter(), true);

                    if (terrainProbe.wasLastOpSuccessful()) {
                        tile.clear();
                        tile.generate();
                    } else {
                        tile.markFailed();
                        this.onGenerationStopped(prevPosition, "RADFG_iGenerationStopped");
                        return;
                    }
                } else {
                    patchFinished = true;
                }
            }
        }
        this.onGenerationStopped(prevPosition, "RADFG_iGenerationDone");
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        // -- generic hotkeys
        theInput.RegisterListener(this, 'OnInteractiveCam', 'RADFG_ToggleInteractiveCam');
        theInput.RegisterListener(this, 'OnGenerate', 'RADFG_GenerateFoliage');
        theInput.RegisterListener(this, 'OnClear', 'RADFG_ClearFoliage');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        // -- generic hotkeys
        theInput.UnregisterListener(this, 'RADFG_ToggleInteractiveCam');
        theInput.UnregisterListener(this, 'RADFG_GenerateFoliage');
        theInput.UnregisterListener(this, 'RADFG_ClearFoliage');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadFG_Sleeping in CRadishFoliageGenerator {}
// ----------------------------------------------------------------------------
statemachine class CRadishFoliageGenerator extends CEntity {
    protected var workContext: CName; default workContext = 'MOD_RadFg_RootState';
    // ------------------------------------------------------------------------
    // UI stuff
    private var confirmPopup: CModUiActionConfirmation;
    private var quitConfirmCallback: CRadFgPopupCallback;
    // ------------------------------------------------------------------------
    private var globalHotKeyMode: bool;
    // ------------------------------------------------------------------------
    private var cameraTracker: CRadishTracker;
    // ------------------------------------------------------------------------
    protected var theCam: CRadishStaticCamera;
    // ------------------------------------------------------------------------
    private var configManager: CRadishFoliageGeneratorConfigManager;
    // ------------------------------------------------------------------------
    protected var generatorParams: CRadishFoliageGeneratorParams;
    protected var foliagePatches: array<CRadFoliagePatch>;
    // ------------------------------------------------------------------------
    protected var area: SRadishRect;
    protected var patchSize: int; default patchSize = 100;  // must be multiple of 25
    // ------------------------------------------------------------------------
    public function init(
        log: CModLogger, configManager: CRadishFoliageGeneratorConfigManager, quitConfirmCallback: CRadFgPopupCallback)
    {
        GetWitcherPlayer().DisplayHudMessage(rqfg_GetLocStringByKeyExt("RADFG_Started"));

        generatorParams = new CRadishFoliageGeneratorParams in this;
        generatorParams.init();

        area = SRadishRect(200, -200, 600, 200);

        this.configManager = configManager;
        this.quitConfirmCallback = quitConfirmCallback;

        this.registerListeners();
        //stateData = (CRadishFgStateData)GetModStorage().load('RadishFoliageGenerator');
        GotoState('RadFG_Sleeping');
    }
    // ------------------------------------------------------------------------
    protected function notice(msg: String) {
        theGame.GetGuiManager().ShowNotification(msg);
    }
    // ------------------------------------------------------------------------
    public function activate() {
        this.theCam = this.createStaticCamera();
        this.cameraTracker = this.createCameraTracker();

        // tracker must be attached to cam because cam position adjusts (invisible)
        // player pos on cam movement and visibility trigger position must be synced
        // with player pos
        theCam.setTracker(cameraTracker);
        theCam.activate();
        theCam.setSettings(configManager.getLastCamPosition());
        theCam.switchTo();

        this.registerListeners();

        theInput.StoreContext(workContext);
        PushState('RadFg_Active');
    }
    // ------------------------------------------------------------------------
    public function deactivate(optional destroyProxies: bool) {
        var proxies: array<CEntity>;
        var i, s: int;

        PopState();
        theInput.RestoreContext(workContext, true);

        this.unregisterListeners();

        if (destroyProxies) {
            theGame.GetEntitiesByTag('RADFG', proxies);
            s = proxies.Size();
            for (i = 0; i < s; i += 1) {
                proxies[i].StopAllEffects();
                proxies[i].Destroy();
            }
        }

        this.cameraTracker.stop();
        this.cameraTracker.Destroy();
        theCam.deactivate();
        theCam.Destroy();
    }
    // ------------------------------------------------------------------------
    public function switchCamTo(placement: SRadishPlacement) {
        theCam.setSettings(placement);
        theCam.switchTo();
    }
    // ------------------------------------------------------------------------
    private function createStaticCamera() : CRadishStaticCamera {
        var template: CEntityTemplate;
        var entity: CEntity;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\static_camera.w2ent", true);
        entity = theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CRadishStaticCamera)entity;
    }
    // ------------------------------------------------------------------------
    private function createCameraTracker() : CRadishTracker {
        var template: CEntityTemplate;
        var entity: CEntity;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\radish_tracker.w2ent", true);
        entity = theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CRadishTracker)entity;
    }
    // ------------------------------------------------------------------------
    protected function getCamTracker() : CRadishTracker {
        return this.cameraTracker;
    }
    // ------------------------------------------------------------------------
    protected function getCamPlacement() : SRadishPlacement {
        return theCam.getSettings();
    }
    // ------------------------------------------------------------------------
    protected function getConfig() : IRadishConfigManager {
        return this.configManager;
    }
    // ------------------------------------------------------------------------
    public function doQuit() {
        configManager.setLastCamPosition(theCam.getSettings());

        //.logDefinition();

        unregisterListeners();
        // to prevent problems with wrongly restored state use this hardcoded
        // "safe" value
        theInput.SetContext('Exploration');

        deactivate(true);
        GetWitcherPlayer().DisplayHudMessage(rqfg_GetLocStringByKeyExt("RADFG_Stopped"));
    }
    // ------------------------------------------------------------------------
    public function quitRequest() {
        var msgTitle: String;
        var msgText: String;

        if (confirmPopup) { delete confirmPopup; }

        confirmPopup = new CModUiActionConfirmation in this;
        msgTitle = "RADFG_tQuitConfirm";
        msgText = "RADFG_mQuitConfirm";

        confirmPopup.open(quitConfirmCallback,
            rqfg_GetLocStringByKeyExt(msgTitle),
            rqfg_GetLocStringByKeyExt(msgText), "quit");
    }
    // ------------------------------------------------------------------------
    event OnQuitRequest(action: SInputAction) {
        if (IsPressed(action)) {
            quitRequest();
        }
    }
    // ------------------------------------------------------------------------
    event OnGlobalModifier(action: SInputAction) {
        if (IsPressed(action)) {
            globalHotKeyMode = true;
        } else if (IsReleased(action)) {
            globalHotKeyMode = false;
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleFoliage(action: SInputAction) {
        if (!globalHotKeyMode && IsPressed(action)) {
            cameraTracker.toggleFoliageVisibilityWithInfo();
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleWater(action: SInputAction) {
        if (!globalHotKeyMode && IsPressed(action)) {
            cameraTracker.toggleWaterVisibilityWithInfo();
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleTerrain(action: SInputAction) {
        if (globalHotKeyMode && IsReleased(action)) {
            cameraTracker.toggleTerrainVisibilityWithInfo();
        }
    }
    // ------------------------------------------------------------------------
    event OnLogDefinition(action: SInputAction) {
        if (IsReleased(action)) {
            this.logDefinition();
            notice(rqfg_GetLocStringByKeyExt("RADFG_iDefinitionLogged"));
        }
    }
    // ------------------------------------------------------------------------
    event OnHelpMePrettyPlease(action: SInputAction) {
        var helpPopup: CModUiHotkeyHelp;
        var titleKey: String;
        var introText: String;
        var hotkeyList: array<SModUiHotkeyHelp>;

        if (IsPressed(action)) {
            helpPopup = new CModUiHotkeyHelp in this;

            titleKey = "RADFG_tHelpHotkey";
            introText = "<p>" + rqfg_GetLocStringByKeyExt("RADFG_mGeneralHelp") + "</p>";

            hotkeyList.PushBack(HotkeyHelp_from('RADFG_GenerateFoliage'));
            hotkeyList.PushBack(HotkeyHelp_from('RADFG_ClearFoliage'));
            hotkeyList.PushBack(HotkeyHelp_from('RADFG_LogDefinition'));
            hotkeyList.PushBack(HotkeyHelp_from('RADFG_Minimize'));
            hotkeyList.PushBack(HotkeyHelp_from('RADFG_ToggleInteractiveCam', 'RAD_ToggleInteractiveCam', IK_LControl));
            hotkeyList.PushBack(HotkeyHelp_from('RADFG_ToggleFoliage', "RAD_ToggleFoliage"));
            hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleWater'));
            hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleTerrain', 'RAD_ToggleTerrain', IK_RControl, IK_F11));
            hotkeyList.PushBack(HotkeyHelp_from('RADFG_Quit'));

            helpPopup.open(titleKey, introText, hotkeyList);
        }
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        // -- generic hotkeys
        theInput.RegisterListener(this, 'OnHelpMePrettyPlease', 'RADFG_ShowHelp');

        theInput.RegisterListener(this, 'OnToggleFoliage', 'RADFG_ToggleFoliage');
        theInput.RegisterListener(this, 'OnToggleWater', 'RAD_ToggleWater');
        theInput.RegisterListener(this, 'OnToggleTerrain', 'RAD_ToggleTerrain');

        theInput.RegisterListener(this, 'OnLogDefinition', 'RADFG_LogDefinition');

        theInput.RegisterListener(this, 'OnGlobalModifier', 'RADFG_ToggleGlobalModifier');
        theInput.RegisterListener(this, 'OnQuitRequest', 'RADFG_Quit');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        // -- generic hotkeys
        theInput.UnregisterListener(this, 'RADFG_ShowHelp');

        theInput.UnregisterListener(this, 'RAD_ToggleFoliage');
        theInput.UnregisterListener(this, 'RAD_ToggleWater');
        theInput.UnregisterListener(this, 'RAD_ToggleTerrain');

        theInput.UnregisterListener(this, 'RADFG_LogDefinition');

        theInput.UnregisterListener(this, 'RADFG_ToggleGlobalModifier');
        theInput.UnregisterListener(this, 'RADFG_Quit');
    }
    // ------------------------------------------------------------------------
    // definition creation
    // ------------------------------------------------------------------------
    public function logDefinition() {
        var patch: CRadFoliagePatch;
        var tile: CRadFoliageMiniTile;
        var definitionWriter: CRadishDefinitionWriter;
        var defs, root, tileDefs: SEncValue;
        var i: int;
        var done: bool;

        root = SEncValue(EEVT_Map);
        defs = SEncValue(EEVT_Map);

        for (i = 0; i < foliagePatches.Size(); i += 1) {

            patch = foliagePatches[i];
            done = !patch.newIter();
            while (!done) {
                tile = patch.next();
                if (tile) {
                    tileDefs = tile.asDefinition();
                    if (tileDefs.m.Size() > 0) {
                        defs.m.PushBack(SEncKeyValue(tile.getId(), tileDefs));
                    }
                } else {
                    done = true;
                }
            }
        }
        root.m.PushBack(SEncKeyValue("foliage", defs));

        definitionWriter = new CRadishDefinitionWriter in this;
        definitionWriter.create('W3FOLIAGE', "Foliage Generator", root);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
