// ----------------------------------------------------------------------------
class CRadFgTerrainProbe extends CRadishTerrainProbe {
    // ------------------------------------------------------------------------
    public function getLandmassIdx(area: SRadishRect) : float {
        var land: float;
        var delta, size: float;

        // heuristic: sample 5 static positions in area
        //  .   .
        //    .
        //  .   .
        size = area.x2 - area.x1;
        delta = size * 0.1;
        size = size * 0.5;

        if (!isWater(Vector(area.x1 + delta, area.y1 + delta))) { land += 1; }
        if (!isWater(Vector(area.x2 - delta, area.y1 + delta))) { land += 1; }
        if (!isWater(Vector(area.x1 + size, area.y1 + size))) { land += 1; }
        if (!isWater(Vector(area.x1 + delta, area.y2 - delta))) { land += 1; }
        if (!isWater(Vector(area.x1 - delta, area.y2 - delta))) { land += 1; }

        return land / 5.0;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
