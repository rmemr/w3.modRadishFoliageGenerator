// ----------------------------------------------------------------------------
state RadFg_InteractiveCamera in CRadishFoliageGenerator extends Rad_InteractiveCamera
{
    default workContext = 'MOD_RadFg_ModeInteractiveCam';
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction) {
        parent.PopState();
    }
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        parent.notice(GetLocStringByKeyExt("RAD_iCamInteractive"));
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        // interactive cam MUST be stopped before changing to static cam!
        theCam.stopInteractiveMode();

        // reactivate the static cam *AFTER* destroying the interactive one
        parent.switchCamTo(theCam.getActiveSettings());

        super.OnLeaveState(nextStateName);
        parent.notice(GetLocStringByKeyExt("RAD_iCamInteractiveStop"));
    }
    // ------------------------------------------------------------------------
    protected function notice(msg: String) {
        parent.notice(msg);
    }
    // ------------------------------------------------------------------------
    protected function createCam() : CRadishInteractiveCamera {
        return createAndSetupInteractiveCam(
            parent.getConfig(), parent.getCamPlacement(), parent.getCamTracker());
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
