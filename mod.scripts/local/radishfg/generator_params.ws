// ----------------------------------------------------------------------------
class CRadishFoliageGeneratorParams {
    private var template1: CEntityTemplate;
    private var template2: CEntityTemplate;

    //TODO probablities, set of trees, set of grass
    // ------------------------------------------------------------------------
    public function init() {
        template1 = (CEntityTemplate)LoadResource("dlc\modtemplates\radishfoliagegenerator\foliage\abies_common_big.w2ent", true);
        template2 = (CEntityTemplate)LoadResource("dlc\modtemplates\radishfoliagegenerator\foliage\abies_common_medium.w2ent", true);
    }
    // ------------------------------------------------------------------------
    //TODO determinisitic seedparam?
    public function getTreeType(terrainHeight: float, terrainSlope: float, terrainType: String) : CEntityTemplate {
        if (RandF() < 0.3) {
            return template1;
        } else {
            return template2;
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
