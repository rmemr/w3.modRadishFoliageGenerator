// ----------------------------------------------------------------------------
struct SRadFoliageInstanceParam {
    var pos: Vector;
    var rot: Float;
    var scale: Float;
}
// ----------------------------------------------------------------------------
struct SRadFoliageInstanceSet {
    var type: String;
    var instances: array<SRadFoliageInstanceParam>;
}
// ----------------------------------------------------------------------------
class CRadFoliagePatch {
    private var area: SRadishRect;
    // ------------------------------------------------------------------------
    private var tiles: array<CRadFoliageMiniTile>;
    private var iterPos: int;
    // ------------------------------------------------------------------------
    public function init(x: int, y: int, size: int, params: CRadishFoliageGeneratorParams) : bool {
        var tile: CRadFoliageMiniTile;

        this.area = SRadishRect(x, y, x + size, y + size);
        for (y = area.y1; y < area.y2; y += 25) {
            for (x = area.x1; x < area.x2; x += 25) {

                tile = new CRadFoliageMiniTile in this;

                if (tile.init(x, y, params)) {
                    this.tiles.PushBack(tile);
                }
            }
        }
        return this.tiles.Size() > 0;
    }
    // ------------------------------------------------------------------------
    public function isDone() : bool {
        var i, s: int;

        s = tiles.Size();
        for (i = 0; i < s; i += 1) {
            if (!tiles[i].isDone()) {
                return false;
            }
        }
        return true;
    }
    // ------------------------------------------------------------------------
    public function getCenter() : Vector {
        var size: float;

        size = 0.5 * (area.x2 - area.x1) ;
        return Vector(area.x1 + size, area.y1 + size);
    }
    // ------------------------------------------------------------------------
    public function clear() {
        var i, s: int;

        s = tiles.Size();
        for (i = 0; i < s; i += 1) {
            tiles[i].clear();
        }
    }
    // ------------------------------------------------------------------------
    //TODO optinal area to iterate only over specific tiles?
    public function newIter(): bool {
        this.iterPos = -1;
        return this.tiles.Size() > 0;
    }
    // ------------------------------------------------------------------------
    public function next() : CRadFoliageMiniTile {
        this.iterPos += 1;
        return this.tiles[this.iterPos];
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
function RadFg_generateId(x: int, y: int) : name {
    // since arbitrary names cannot be generated just use one of 4 "sector"-ids
    if (x < 0) {
        if (y < 0) { return 'radfgid_11'; } else { return 'radfgid_12'; }
    } else {
        if (y < 0) { return 'radfgid_21'; } else { return 'radfgid_22'; }
    }
}
// ----------------------------------------------------------------------------
function RadFg_getFoliageEntities(area: SRadishRect): array<CEntity> {
    var result: array<CEntity>;
    var bytag: array<CEntity>;
    var id1, id2: name;
    var i, j, s: int;
    var pos: Vector;
    var sectors: array<name>;

    id1 = RadFg_generateId(area.x1, area.y1);
    id2 = RadFg_generateId(area.x2, area.y2);

    // if both ids are identical the whole area is in the same quadrant
    // should be the most common use case -> optimize
    if (id1 == id2) {
        sectors.PushBack(id1);
    } else {
        if (id1 == 'radfgid_11' && id2 == 'radfgid_22') {
            // crossing all 4 sectors
            sectors.PushBack('radfgid_11');
            sectors.PushBack('radfgid_12');
            sectors.PushBack('radfgid_21');
            sectors.PushBack('radfgid_22');
        } else {
            // crossing only 2 sectors
            sectors.PushBack(id1);
            sectors.PushBack(id2);
        }
    }

    for (j = 0; j < sectors.Size(); j += 1) {
        bytag.Clear();
        theGame.GetEntitiesByTag(sectors[j], bytag);

        s = bytag.Size();
        for (i = 0; i < s; i += 1) {
            pos = bytag[i].GetWorldPosition();

            // filter only the entities in the queried area
            if (area.x1 <= pos.X && pos.X < area.x2 && area.y1 <= pos.Y && pos.Y < area.y2) {
                result.PushBack(bytag[i]);
            }
        }
    }

    return result;
}
// ----------------------------------------------------------------------------
class CRadFoliageMiniTile {
    private var params: CRadishFoliageGeneratorParams;
    private var id: name;
    private var center: Vector;

    private var terrainProbe: CRadFgTerrainProbe;
    private var area: SRadishRect;
    private var landMassIdx: float;

    private var foliage: array<SRadFoliageInstanceSet>;
    private var foliageSlots: array<String>;

    private var done: bool;
    private var failed: bool;
    // ------------------------------------------------------------------------
    public function init(x: int, y: int, params: CRadishFoliageGeneratorParams) : bool {
        this.params = params;
        this.area = SRadishRect(x, y, x + 25, y + 25);
        this.center = Vector(x + 12.5, y + 12.5);

        this.id = RadFg_generateId(x, y);

        terrainProbe = new CRadFgTerrainProbe in this;
        landMassIdx = terrainProbe.getLandmassIdx(this.area);

        failed = false;
        return landMassIdx > 0.0;
    }
    // ------------------------------------------------------------------------
    public function getCenter() : Vector {
        return this.center;
    }
    // ------------------------------------------------------------------------
    public function getId() : String {
        return "tile_" + area.x1 + "x" + area.y1 + "_" + area.x2 + "x" + area.y2;
    }
    // ------------------------------------------------------------------------
    public function clear() {
        var entities: array<CEntity>;
        var i, s: int;

        // delete spawned by tag
        entities = RadFg_getFoliageEntities(area);

        s = entities.Size();
        for (i = 0; i < s; i += 1) {
            entities[i].Destroy();
        }
        entities.Clear();
        foliage.Clear();
        foliageSlots.Clear();
        done = false;
    }
    // ------------------------------------------------------------------------
    public function generate() {
        var template: CEntityTemplate;
        var entity: CEntity;
        var basePos, groundPos, groundNormal: Vector;
        var rot: EulerAngles;
        var i, s: int;
        var x, y: int;
        var density: int;
        var scale: float;
        var foliageSlot: int;

        var targetPos: Vector;

        //LogChannel('FG', "generating for tile " + area.x1 + "x" + area.y1);

        basePos = Vector(area.x1, area.y1, 0.0);
        density = 4;

        for (y = 0; y < 25; y = y + density) {
            for (x = 0; x < 25; x = x + density) {

                // RandNoiseF( seed : int, max : float, optional min : float ) : float;
                //FIXME: probablities
                if (RandF() > 0.6) {
                    targetPos = basePos + Vector(x + RandF(), y + RandF(), 0.0);

                    if (terrainProbe.isWater(targetPos)) {
                        continue;
                    }
                    if (terrainProbe.getGroundPos(targetPos, groundPos, groundNormal)) {
                        rot = EulerAngles(0.0, 360.0 * RandF(), 0.0);
                        scale = 0.5 + RandF();

                        template = params.getTreeType(groundPos.Z, 0.0, "");
                        entity = theGame.CreateEntity(template, groundPos, rot);
                        entity.AddTag(this.id);
                        entity.AddTag('RADFG');

                        //TODO: some kind of scaling?

                        // collect generated params
                        foliageSlot = foliageSlots.FindFirst(template.GetPath());
                        if (foliageSlot == -1) {
                            // new template
                            foliageSlots.PushBack(template.GetPath());
                            foliageSlot = foliageSlots.Size() - 1;

                            foliage.PushBack(SRadFoliageInstanceSet(extractFoliageId(template.GetPath())));
                        }
                        foliage[foliageSlot].instances.PushBack(SRadFoliageInstanceParam(groundPos, rot.Yaw, scale));
                    }
                }
            }
        }
        done = true;
    }
    // ------------------------------------------------------------------------
    public function extractFoliageId(path: String): String {
        var tmp, filename: String;

        StrSplitLast(path, StrChar(92), tmp, filename);
        return StrReplace(filename, ".w2ent", "");
    }
    // ------------------------------------------------------------------------
    public function markFailed() {
        failed = true;
        LogChannel('FG', "tile " + getId() + " marked as failed");
    }
    // ------------------------------------------------------------------------
    public function isDone() : bool {
        return done || failed;
    }
    // ------------------------------------------------------------------------
    // definition creation
    // ------------------------------------------------------------------------
    public function asDefinition() : SEncValue {
        var foliageType: SRadFoliageInstanceSet;
        var params: SRadFoliageInstanceParam;
        var content, foliageSet, foliageInstance: SEncValue;
        var i, f, s: int;

        content = SEncValue(EEVT_Map);

        for (i = 0; i < foliage.Size(); i += 1) {
            foliageType = foliage[i];
            s = foliageType.instances.Size();

            if (s > 0) {
                foliageSet = SEncValue(EEVT_List);
                foliageSet.l.Clear();

                for (f = 0; f < s; f += 1) {
                    foliageInstance = SEncValue(EEVT_List);
                    foliageInstance.l.Clear();

                    params = foliageType.instances[f];

                    foliageInstance.l.PushBack(FloatToEncValue(params.pos.X));
                    foliageInstance.l.PushBack(FloatToEncValue(params.pos.Y));
                    foliageInstance.l.PushBack(FloatToEncValue(params.pos.Z));
                    foliageInstance.l.PushBack(FloatToEncValue(params.scale));
                    foliageInstance.l.PushBack(FloatToEncValue(params.rot));

                    foliageSet.l.PushBack(foliageInstance);
                }

                content.m.PushBack(SEncKeyValue(foliageType.type, foliageSet));
                content.m.PushBack(SeperatorToEncKeyValue());
            }
        }
        return content;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
