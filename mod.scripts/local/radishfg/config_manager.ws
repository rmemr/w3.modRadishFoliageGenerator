// ----------------------------------------------------------------------------
// DO NOT CHANGE PROPERTY ORDER (important for initialization!)
// ----------------------------------------------------------------------------
struct SRadishFgConfig {
    var cam: SRadishCamConfig;
    var placement: SRadishPlacementConfig;
}
// ----------------------------------------------------------------------------
class CRadishFoliageGeneratorConfigManager extends IRadishConfigManager {
    protected var config: SRadishFgConfig;
    // ------------------------------------------------------------------------
    public function init() {
        config = getDefaults();
    }
    // ------------------------------------------------------------------------
    private function getDefaults() : SRadishFgConfig {
        return SRadishFgConfig(
            // DO NOT CHANGE ORDER (important for initialization!)
            SRadishCamConfig(
                SRadishInteractiveStepConfig(0.05, 0.05),
                SRadishInteractiveStepConfig(0.25, 0.10),
                SRadishInteractiveStepConfig(0.80, 0.20),
                true,
                RadUi_createCamSettingsFor(RadUiCam_Empty)
            ),
            SRadishPlacementConfig(
                SRadishInteractiveStepConfig(0.01, 0.1),
                SRadishInteractiveStepConfig(0.03, 0.2),
                SRadishInteractiveStepConfig(0.1, 0.3),
            )
        );
    }
    // ------------------------------------------------------------------------
    // setter
    // ------------------------------------------------------------------------
    public function setLastCamPosition(placement: SRadishPlacement) {
        config.cam.lastPos = placement;
    }
    // ------------------------------------------------------------------------
    // getter
    // ------------------------------------------------------------------------
    public function getLastCamPosition(): SRadishPlacement {
        return config.cam.lastPos;
    }
    // ------------------------------------------------------------------------
    public function getCamConfig() : SRadishCamConfig {
        return config.cam;
    }
    // ------------------------------------------------------------------------
    public function getPlacementConfig() : SRadishPlacementConfig {
        return config.placement;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
